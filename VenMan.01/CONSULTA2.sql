----Historico de ingresos por productos anuales.
SELECT PRODUCTO.NOMBRE_PRODUCTO, EXTRACT (YEAR FROM PRODUCTO.FECHA_PRODUCTO)AS FECHA, SUM (VENTA.TOTAL_VENTA)AS TOTAL FROM PRODUCTO
INNER JOIN RELATIONSHIP_4 ON PRODUCTO.PRODUCTOS_ID = RELATIONSHIP_4.PRODUCTOS_ID
INNER JOIN VENTA ON RELATIONSHIP_4.VENTAS_ID = VENTA.VENTAS_ID 
GROUP BY EXTRACT (YEAR FROM PRODUCTO.FECHA_PRODUCTO), PRODUCTO.NOMBRE_PRODUCTO
ORDER BY EXTRACT (YEAR FROM PRODUCTO.FECHA_PRODUCTO)